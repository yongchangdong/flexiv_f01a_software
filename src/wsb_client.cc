#include <wsb_client.h>
#include "log.h"
#include <thread>
#include <chrono>
#include <iostream>
#include <string.h>
#include "tiny_ftp_transfer.h"

constexpr int kMaxQueueSize = 1;

WsbClient::WsbClient(const Socket &socket) : Socket(socket),
                                             state_(State::kClose)
{
  wsb_msg__init(&package_.wsb_msg);
  test_sensor__init(&package_.test_sensor);
  system__init(&package_.system);
  ftp__init(&package_.ftp);
}

WsbClient::~WsbClient() {}

bool WsbClient::RecvData(struct package_t *client, uint8_t data)
{
  switch (client->unpack_state)
  {
  case 0:
  {
    /* Find pack head */
    if (data == PACK_HEAD)
    {
      client->unpack_state = 1;
    }
    break;
  }
  case 1:
  {
    /* Get pack length */
    client->msg.len = data;
    /* Check msg len is less than buff size or msg len is not zero */
    if (client->msg.len > MAX_BUFF_SIZE || client->msg.len == 0)
    {
      client->unpack_state = 0;
    }
    else
    {
      client->unpack_state = 2;
      client->data_len = 0;
    }
    break;
  }
  case 2:
  {
    /* Recv data */
    client->msg.data[client->data_len++] = data;
    if (client->msg.len == client->data_len)
    {
      /* Recv complete*/
      client->unpack_state = 0;
      return true; // only when msg.data has been filled entirely, return true
    }
    break;
  }
  default:
  {
    break;
  }
  }

  return false;
}

int WsbClient::Pack(struct package_t *package, WsbMsg__MsgType type)
{
  /* Copy data */
  switch (type)
  {
  case WSB_MSG__MSG_TYPE__kSystem:
  {
    /* Check pack size */
    if (system__get_packed_size(&package->system) > MAX_BUFF_SIZE)
      return -1;
    /* Pack sensor informat to buff */
    package->buff_len = (int)system__pack(&package->system, package->buff);
    break;
  }
  case WSB_MSG__MSG_TYPE__kTestSensor:
  {
    /* Check pack size */
    if (test_sensor__get_packed_size(&package->test_sensor) > MAX_BUFF_SIZE)
      return -1;
    /* Pack test sensor information to buff */
    package->buff_len = (int)test_sensor__pack(&package->test_sensor, package->buff);
    break;
  }
  case WSB_MSG__MSG_TYPE__kFtp:
  {
    /* Check pack size */
    if (ftp__get_packed_size(&package->ftp) > MAX_BUFF_SIZE)
      return -1;
    /* Pack sensor informat to buff */
    package->buff_len = (int)ftp__pack(&package->ftp, package->buff);
    break;
  }
  default:
  {
    /* Unknow message type */
    return -1;
    break;
  }
  }

  /* Copy data */
  package->wsb_msg.msg_type = type;
  package->wsb_msg.embed_msg.len = package->buff_len;
  package->wsb_msg.embed_msg.data = package->buff;

  /* Check pack size */
  if (wsb_msg__get_packed_size(&package->wsb_msg) > MAX_BUFF_SIZE)
  {
    return -1;
  }
  /* Pack buff to message */
  package->msg.len = (uint8_t)wsb_msg__pack(&package->wsb_msg, package->msg.data);

  return 0;
}

WsbMsg__MsgType WsbClient::PackType(struct package_t *package)
{
  /* Unpack message */
  struct _WsbMsg *wsb_msg_ptr = wsb_msg__unpack(NULL, package->msg.len, package->msg.data);

  if (wsb_msg_ptr == NULL)
    return WsbMsg__MsgType(-1);

  return wsb_msg_ptr->msg_type;
}

struct _Sensor *WsbClient::UnpackSensor(struct package_t *package)
{
  /* Unpack message */
  struct _WsbMsg *wsb_msg_ptr = wsb_msg__unpack(NULL, package->msg.len, package->msg.data);

  if (wsb_msg_ptr == NULL)
    return NULL;

  /* Copy and free resource */
  memcpy(&package->wsb_msg, wsb_msg_ptr, sizeof(struct _WsbMsg));

  package->buff_len = (int)wsb_msg_ptr->embed_msg.len;
  memcpy(&package->buff, wsb_msg_ptr->embed_msg.data, wsb_msg_ptr->embed_msg.len);
  wsb_msg__free_unpacked(wsb_msg_ptr, NULL);

  return sensor__unpack(NULL, package->buff_len, package->buff);
}

struct _TestSensor *WsbClient::UnpackTestSensor(struct package_t *package)
{
  /* Unpack message */
  struct _WsbMsg *wsb_msg_ptr = wsb_msg__unpack(NULL, package->msg.len, package->msg.data);

  if (wsb_msg_ptr == NULL)
    return NULL;

  /* Copy and free resource */
  memcpy(&package->wsb_msg, wsb_msg_ptr, sizeof(struct _WsbMsg));

  package->buff_len = (int)wsb_msg_ptr->embed_msg.len;
  memcpy(&package->buff, wsb_msg_ptr->embed_msg.data, wsb_msg_ptr->embed_msg.len);
  wsb_msg__free_unpacked(wsb_msg_ptr, NULL);

  return test_sensor__unpack(NULL, package->buff_len, package->buff);
}

struct _System *WsbClient::UnpackSystem(struct package_t *package)
{
  /* Unpack message */
  struct _WsbMsg *wsb_msg_ptr = wsb_msg__unpack(NULL, package->msg.len, package->msg.data);

  if (wsb_msg_ptr == NULL)
    return NULL;

  /* Copy and free resource */
  memcpy(&package->wsb_msg, wsb_msg_ptr, sizeof(struct _WsbMsg));

  package->buff_len = (int)wsb_msg_ptr->embed_msg.len;
  memcpy(&package->buff, wsb_msg_ptr->embed_msg.data, wsb_msg_ptr->embed_msg.len);
  wsb_msg__free_unpacked(wsb_msg_ptr, NULL);

  return system__unpack(NULL, package->buff_len, package->buff);
}

struct _Ftp *WsbClient::UnpackFtp(struct package_t *package)
{
  /* Unpack message */
  struct _WsbMsg *wsb_msg_ptr = wsb_msg__unpack(NULL, package->msg.len, package->msg.data);

  if (wsb_msg_ptr == NULL)
    return NULL;

  /* Copy and free resource */
  memcpy(&package->wsb_msg, wsb_msg_ptr, sizeof(struct _WsbMsg));

  package->buff_len = (int)wsb_msg_ptr->embed_msg.len;
  memcpy(&package->buff, wsb_msg_ptr->embed_msg.data, wsb_msg_ptr->embed_msg.len);
  wsb_msg__free_unpacked(wsb_msg_ptr, NULL);

  return ftp__unpack(NULL, package->buff_len, package->buff);
}

int ThreadFun(WsbClient *client)
{
  client->state_ = WsbClient::State::kClose;

  logger::debug(logger::Level::Debug) << "wsb thread entry";

  /* Open socket */
  if (!client->open())
  {
    logger::debug(logger::Level::Error) << "Open socket error";
    client->state_ = WsbClient::State::kClose;
    return -1;
  }

  /* Set time out 10ms */
  struct timeval tv_timeout;
  tv_timeout.tv_sec = 0;
  tv_timeout.tv_usec = 10000;
  int rel = ::setsockopt(client->fd, SOL_SOCKET, SO_RCVTIMEO, &tv_timeout, sizeof(tv_timeout));
  if (rel)
  {
    logger::debug(logger::Level::Error) << "Socket set timeout error";
    logger::debug(logger::Level::Debug) << "wsb thread quit";
    return -2;
  }

  logger::debug(logger::Level::Info) << "Connecting to server, please waiting.....";
  client->state_ = WsbClient::State::kConnecting;

  /* Connect to server */
  if (!client->connect())
  {
    logger::debug(logger::Level::Error) << "Connect socket error";
    client->close();
    logger::debug(logger::Level::Debug) << "wsb thread quit";
    client->state_ = WsbClient::State::kClose;
    return -3;
  }

  logger::debug(logger::Level::Info) << "Connected";
  client->state_ = WsbClient::State::kConnected;

  /* Recv server msg */
  while (1)
  {
    if (client->isConnected())
    {
      unsigned char buff[512];
      int numbytes = (int)client->recv(buff, 512);
      if (numbytes > 0)
      {
        for (int i = 0; i < numbytes; i++)
        {
          if (client->RecvData(&client->package_, buff[i]))
          {
            switch (client->PackType(&client->package_))
            {
            case WSB_MSG__MSG_TYPE__kSystem:
            {
              struct _System *system_ptr = client->UnpackSystem(&client->package_);
              if (system_ptr)
              {
                if (client->on_system_.operator bool())
                  client->on_system_(system_ptr);
                system__free_unpacked(system_ptr, NULL);
              }
              break;
            }
            case WSB_MSG__MSG_TYPE__kTestSensor:
            {
              struct _TestSensor *test_sensor_ptr = client->UnpackTestSensor(&client->package_);
              if (test_sensor_ptr)
              {
                if (client->on_test_data_.operator bool())
                {
                  client->on_test_data_(test_sensor_ptr);
                }

                test_sensor__free_unpacked(test_sensor_ptr, NULL);
              }
              else
              {
                logger::debug(logger::Level::Warning) << "Unpack test sensor data fail";
              }
              break;
            }
            case WSB_MSG__MSG_TYPE__kSensor:
            {
              struct _Sensor *sensor_ptr = client->UnpackSensor(&client->package_);
              if (sensor_ptr)
              {
                if (client->on_data_.operator bool())
                  client->on_data_(sensor_ptr);

                sensor__free_unpacked(sensor_ptr, NULL);
              }
              else
              {
                logger::debug(logger::Level::Warning) << "Unpack sensor data fail";
              }
              break;
            }
            case WSB_MSG__MSG_TYPE__kFtp:
            {
              struct _Ftp *ftp_ptr = client->UnpackFtp(&client->package_);
              if (ftp_ptr)
              {

                client->FtpHandle(ftp_ptr);
                ftp__free_unpacked(ftp_ptr, NULL);
              }
              break;
            }
            default:
            {
              logger::debug(logger::Level::Warning) << "Unknow pack type";
              break;
            }
            }
          }
        }
      }
      else if (numbytes == -1)
      {
        client->TimeoutCb();
      }
      else
      {
        logger::debug() << "Unknow error, thread quit" << numbytes;
        client->state_ = WsbClient::State::kClose;
        return -4;
      }
    }
    else
    {
      logger::debug() << "Disconnect, thread quit ";
      client->state_ = WsbClient::State::kClose;
      return -5;
    }
  }
  logger::debug(logger::Level::Debug) << "wsb thread quit";
  return 0;
}

bool WsbClient::Start()
{
  if (state_ != State::kClose)
  {
    logger::debug(logger::Level::Info) << "Socket is busy, please close before open";
    return false;
  }

  if (thread_.joinable())
    thread_.join();

  thread_ = std::thread(ThreadFun, this);

  return true;
}

bool WsbClient::Stop()
{
  if (state_ != State::kConnected)
  {
    logger::debug(logger::Level::Info) << "Socket is Disconnected";
  }
  else
  {
    close();
  }

  if (thread_.joinable())
    thread_.join();
  return true;
}

bool WsbClient::SendSystem(System__CmdType cmd,
                           int32_t param_1,
                           int32_t param_2,
                           int32_t param_3,
                           int32_t param_4,
                           int32_t param_5)
{
  package_.system.cmd = cmd;
  package_.system.param_1 = param_1;
  package_.system.param_2 = param_2;
  package_.system.param_3 = param_3;
  package_.system.param_4 = param_4;
  package_.system.param_5 = param_5;

  if (isConnected())
  {
    if (0 == Pack(&package_, WSB_MSG__MSG_TYPE__kSystem))
    {
      Socket::send((void *)&package_.msg, package_.msg.len + 2);
    }
  }
  else
    return false;
  return true;
}

void WsbClient::FtpHandle(struct _Ftp *ftp)
{
  TinyFtpTransfer_Read(this, ftp->data.data, ftp->data.len);
}

bool WsbClient::SendFtp(uint32_t len, uint8_t *data)
{
  if (isConnected())
  {
    package_.ftp.data.data = data;
    package_.ftp.data.len = len;
    if (0 == Pack(&package_, WSB_MSG__MSG_TYPE__kFtp))
    {
      Socket::send((void *)&package_.msg, package_.msg.len + 2);
    }
  }
  else
    return false;
  return true;
}

void WsbClient::TimeoutCb()
{
  TinyFtpTransfer_TimePoll();
}

void WsbClient::Register_OnSystem(std::function<void(_System *)> on_system)
{
  on_system_ = on_system;
}

void WsbClient::Register_OnData(std::function<void(_Sensor *)> on_data)
{
  on_data_ = on_data;
}

void WsbClient::Register_OnTestData(std::function<void(_TestSensor *)> on_test_data)
{
  on_test_data_ = on_test_data;
}