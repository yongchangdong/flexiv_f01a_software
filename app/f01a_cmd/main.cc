﻿// flexiv_gripper.cpp: 定义应用程序的入口点。
//
#include <wsb_client.h>
#include <iostream>
#include <chrono>
#include "log.h"
#include "cmdline.h"
#include "tiny_ftp_transfer.h"
#include <thread>
#include <chrono>

using namespace std;
using namespace cmdline;

cmdline::parser cmd_quit;
cmdline::parser cmd_system;
cmdline::parser cmd_version;

cmdline::parser cmd_ftp;
cmdline::parser cmd_reboot;
cmdline::parser cmd_get_network;
cmdline::parser cmd_set_network;

cmdline::parser cmd_publish_interval;
cmdline::parser cmd_sensor_data;

cmdline::parser cmd_gain;
cmdline::parser cmd_offset;

void OnData(_Sensor *sensor);
void OnSystem(_System *system);

void CommandLineInit()
{
  /* Stop client, example: quit */
  cmd_quit.set_program_name("quit");
  cmd_quit.add<int>("delay", 'd', "delay and quit", false, 0, cmdline::range(0, 1000000));

  /* System command, example : system */
  cmd_system.set_program_name("system");
  cmd_system.add("help", 0, "Request get state word and fault code");

  /* interval command, example : interval --cycle=100 */
  cmd_publish_interval.set_program_name("interval");
  cmd_publish_interval.add<int>("cycle", 'c', "update cycle in ms", true, 1000, cmdline::range(0, 100000));

  /* version command, example : version */
  cmd_version.set_program_name("version");

  /* Ftp command, example : ftp --file=/home/mnt/test.bin */
  cmd_ftp.set_program_name("ftp");
  cmd_ftp.add<string>("file", 'f', "files", true, "");

  /* Reboot system, example: reboot */
  cmd_reboot.set_program_name("reboot");

  cmd_get_network.set_program_name("network");

  /* Set ip, gate way and netmask, example: ip --ip_addr=192.168.0.5 --gateway=192.168.0.5 --netmask=255.255.255.0 */
  cmd_set_network.set_program_name("set-network");
  cmd_set_network.add<string>("ip_addr", 'i', "ip address", true, "192.168.0.5");
  cmd_set_network.add<string>("gateway", 'g', "gate way", true, "255.255.255.0");
  cmd_set_network.add<string>("netmask", 'm', "net mask", true, "192.168.0.1");

  /* cmd_get_gain, example : gain */
  cmd_gain.set_program_name("gain");

  /* cmd_get_offset, example : offset */
  cmd_offset.set_program_name("offset");
}

void IPConvert(const char *ip, unsigned int &ip_addr)
{
  char scIPAddress[32] = "192.168.1.151";
  unsigned int nIPAddress = 0;
  int nTmpIP[4] = {0};
  int i = 0;

  sscanf(ip, "%d.%d.%d.%d", &nTmpIP[0], &nTmpIP[1], &nTmpIP[2], &nTmpIP[3]);
  for (i = 0; i < 4; i++)
  {
    nIPAddress += (nTmpIP[i] << (24 - (i * 8)) & 0xFFFFFFFF);
  }

  printf("IP：%u\r\n", nIPAddress);
  ip_addr = nIPAddress;

  sprintf(scIPAddress, "%d.%d.%d.%d", nIPAddress >> 24, (nIPAddress & 0xFF0000) >> 16, (nIPAddress & 0xFF00) >> 8, nIPAddress & 0xFF);
  printf("IP：%s\r\n", scIPAddress);

  return;
}

void IPConvert2Str(uint32_t ip, std::string &ip_str)
{
  uint32_t ip_int_index[4];
  char ip_table[48];
  uint32_t ip_temp = 24;

  for (int j = 0; j < 4; j++)
  {
    ip_int_index[j] = (ip >> ip_temp) & 0xFF;
    ip_temp -= 8;
  }

  sprintf(ip_table, "%d.%d.%d.%d", ip_int_index[3], ip_int_index[2], ip_int_index[1], ip_int_index[0]);
  ip_str = std::string(ip_table);
}

int main(int argc, char *argv[])
{
  /* WSB tcp client init */
  logger::debug() << "F01A cmd line controller";

  if (argc < 2)
  {
    logger::debug() << "Example : force_sensor_cmd 192.168.0.5";

    exit(1);
  }

  Socket::Address address;
  address = Socket::Address::Ipv4(argv[1], 1000);
  WsbClient client(Socket::Tcp(address));

  client.Register_OnSystem(OnSystem);
  client.Register_OnData(OnData);

  /* Start */
  logger::debug() << "Start client";

  if (false == client.Start())
  {
    logger::debug(logger::Level::Warning) << "Start fail";
    return -1;
  }

  std::chrono::system_clock::time_point start_time = std::chrono::system_clock::now();

  while (1)
  {
    if (true == client.isConnected())
    {
      break;
    }

    std::chrono::system_clock::time_point current = std::chrono::system_clock::now();

    unsigned int time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                                    std::chrono::system_clock::now() - start_time)
                                    .count();

    if (time_elapsed > 2000)
    {
      logger::debug() << "time out";
      client.Stop();
      exit(-1);
    }
  }

  logger::debug() << "f01a is ready";

  CommandLineInit();

  while (1)
  {
    char buff[256];
    cin.getline(buff, sizeof(buff));

    /* quit */
    {
      bool ok = cmd_quit.parse(std::string(buff));
      if (ok)
      {
        logger::debug() << "Quit";
        exit(0);
        continue;
      }
    }

    /* system */
    {
      bool ok = cmd_system.parse(std::string(buff));
      if (ok)
      {
        logger::debug() << "Recv system command";
        client.SendSystem(SYSTEM__CMD_TYPE__kSystem, 0, 1, 2, 3, 4);
        continue;
      }
    }

    /* interval */
    {
      bool ok = cmd_publish_interval.parse(std::string(buff));
      if (ok)
      {
        logger::debug() << "interval "
                        << "cycle=" << cmd_publish_interval.get<int>("cycle");
        client.SendSystem(SYSTEM__CMD_TYPE__kSetInterval, cmd_publish_interval.get<int>("cycle"));
        continue;
      }
    }

    /* version */
    {
      bool ok = cmd_version.parse(std::string(buff));
      if (ok)
      {
        logger::debug() << "version";
        client.SendSystem(SYSTEM__CMD_TYPE__kVersion);
        continue;
      }
    }

    /* remote upgrade */
    {
      bool ok = cmd_ftp.parse(std::string(buff));
      if (ok)
      {
        logger::debug() << "ftp";
        string file_name = cmd_ftp.get<string>("file");
        TinyFtpTransfer_SendFile(&client, file_name.c_str());
        file_name;
        continue;
      }
    }

    /* reboot */
    {
      bool ok = cmd_reboot.parse(std::string(buff));
      if (ok)
      {
        client.SendSystem(SYSTEM__CMD_TYPE__kReboot);
        continue;
      }
    }

    /* set ip address */
    {
      bool ok = cmd_set_network.parse(std::string(buff));
      if (ok)
      {
        logger::debug() << "Network";
        string ip_addr_str = cmd_set_network.get<string>("ip_addr");
        string gate_way_str = cmd_set_network.get<string>("gateway");
        string netmask_str = cmd_set_network.get<string>("netmask");

        logger::debug() << "ip address : " << ip_addr_str;
        logger::debug() << "gate way : " << gate_way_str;
        logger::debug() << "netmask :" << netmask_str;

        unsigned int ip_addr;
        unsigned int gate_way;
        unsigned int netmask;
        IPConvert(ip_addr_str.c_str(), ip_addr);
        IPConvert(gate_way_str.c_str(), gate_way);
        IPConvert(netmask_str.c_str(), netmask);

        client.SendSystem(SYSTEM__CMD_TYPE__kSetIP4, ip_addr, gate_way, netmask);

        continue;
      }
    }

    /* get network */
    {
      bool ok = cmd_get_network.parse(std::string(buff));
      if (ok)
      {
        client.SendSystem(SYSTEM__CMD_TYPE__kIP4);
        continue;
      }
    }

    /* get network */
    {
      bool ok = cmd_gain.parse(std::string(buff));
      if (ok)
      {
        client.SendSystem(SYSTEM__CMD_TYPE__kHallGain);
        continue;
      }
    }

    /* get network */
    {
      bool ok = cmd_offset.parse(std::string(buff));
      if (ok)
      {
        client.SendSystem(SYSTEM__CMD_TYPE__kHallOffset);
        continue;
      }
    }
  }

  return EXIT_SUCCESS;
}

void OnData(_Sensor *sensor)
{
  logger::debug(logger::Level::Info) << "tick :" << sensor->tick << ",fz : " << sensor->fz;
}

void OnSystem(_System *system)
{
  /* Print */
  logger::debug(logger::Level::Info)
      << "cmd type : " << system->cmd << ", params<" << system->param_1 << "," << system->param_2 << "," << system->param_3 << "," << system->param_4 << "," << system->param_5 << ">";

  switch (system->cmd)
  {
  case SYSTEM__CMD_TYPE__kVersion:
  {
    logger::debug(logger::Level::Info)
        << "firmware version : " << system->param_1 << "." << system->param_2 << "." << system->param_3;
    break;
  }
  case SYSTEM__CMD_TYPE__kIP4:
  {
    uint32_t ip_addr_ = system->param_1;
    uint32_t ip_mask_ = system->param_2;
    uint32_t ip_gw_ = system->param_3;

    std::string ip_ram;
    IPConvert2Str(ip_addr_, ip_ram);
    logger::debug(logger::Level::Info) << "ip address " << ip_ram;
    IPConvert2Str(ip_mask_, ip_ram);
    logger::debug(logger::Level::Info) << "ip mask " << ip_ram;
    IPConvert2Str(ip_gw_, ip_ram);
    logger::debug(logger::Level::Info) << "gate way " << ip_ram;
    break;
  }
  case SYSTEM__CMD_TYPE__kInterval:
  {
    logger::debug(logger::Level::Info) << "sensor data publish interval " << system->param_1;
    break;
  }
  case SYSTEM__CMD_TYPE__kAccelerometer:
  {
    break;
  }
  case SYSTEM__CMD_TYPE__kHallGain:
  {
    logger::debug(logger::Level::Info) << "hall gain 1 : " << system->param_1;
    logger::debug(logger::Level::Info) << "hall gain 2 : " << system->param_2;
    logger::debug(logger::Level::Info) << "hall gain 3 : " << system->param_3;
    logger::debug(logger::Level::Info) << "hall gain 4 : " << system->param_4;

    break;
  }
  case SYSTEM__CMD_TYPE__kHallOffset:
  {
    logger::debug(logger::Level::Info) << "hall gain offset 1 : " << system->param_1;
    logger::debug(logger::Level::Info) << "hall gain offset 2 : " << system->param_2;
    logger::debug(logger::Level::Info) << "hall gain offset 3 : " << system->param_3;
    logger::debug(logger::Level::Info) << "hall gain offset 4 : " << system->param_4;

    break;
  }
  case SYSTEM__CMD_TYPE__kTestInterval:
  {
    logger::debug(logger::Level::Info) << "test sensor data publish interval : " << system->param_1;
    logger::debug(logger::Level::Info) << "test sensor data publish interval 2 : " << system->param_2;
    logger::debug(logger::Level::Info) << "test sensor data publish interval 3 : " << system->param_3;
    logger::debug(logger::Level::Info) << "test sensor data publish interval 4 : " << system->param_4;

    break;
  }
  case SYSTEM__CMD_TYPE__kGyroscope:
  {
    break;
  }
  default:
    break;
  }
}