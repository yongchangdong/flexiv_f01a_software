/**
 * @file mainwindow.cpp
 * @copyright Flexiv Robotics 2021
 * @brief F01A testing GUI
 * @author Jinzhao
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QTextStream>
#include <iostream>
#include <regex>
#include <tiny_ftp_transfer.h>
#include <config.h>

/** Hall gain factor */
constexpr float kHallGainFactor = 10000.0f;

/**
 * @brief Convert IP address to uint32_t type from string type
 * @param[in] ip IP E.g 192.168.0.5
 * @param[out] ip_addr IP address
 */
void IPConvert(const char *ip, unsigned int &ip_addr)
{
    char scIPAddress[32] = "192.168.1.151";
    unsigned int nIPAddress = 0;
    int nTmpIP[4] = {0};
    int i = 0;

    sscanf(ip, "%d.%d.%d.%d", &nTmpIP[0], &nTmpIP[1], &nTmpIP[2], &nTmpIP[3]);
    for (i = 0; i < 4; i++)
    {
        nIPAddress += (nTmpIP[i] << (24 - (i * 8)) & 0xFFFFFFFF);
    }

    printf("Convert IP：%u\r\n", nIPAddress);
    ip_addr = nIPAddress;

    sprintf(scIPAddress, "%d.%d.%d.%d", nIPAddress >> 24, (nIPAddress & 0xFF0000) >> 16, (nIPAddress & 0xFF00) >> 8, nIPAddress & 0xFF);
    printf("Convert IP：%s\r\n", scIPAddress);

    return;
}

/**
 * @brief Convert IP address to  string type from uint32_t type
 * @param[in] ip IP
 * @param[out] ip_str IP address
 */
void IPConvert2Str(uint32_t ip, std::string &ip_str)
{
    uint32_t ip_int_index[4];
    char ip_table[48];
    uint32_t ip_temp = 24;

    for (int j = 0; j < 4; j++)
    {
        ip_int_index[j] = (ip >> ip_temp) & 0xFF;
        ip_temp -= 8;
    }

    sprintf(ip_table, "%d.%d.%d.%d", ip_int_index[3], ip_int_index[2], ip_int_index[1], ip_int_index[0]);
    ip_str = std::string(ip_table);
}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
                                          ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle(QString("F01A GUI - ") + QString::fromStdString(FLEXIV_F01A_SOFTWARE_VERSION));

    /* setup graphs and other settings */
    SetupGUI();

    client_ptr_ = std::make_shared<WsbClient>(Socket::Tcp(Socket::Address::Ipv4("192.168.0.5", 80)));

    /* register callback function */
    client_ptr_->Register_OnSystem(std::bind(&MainWindow::OnSystem, this, std::placeholders::_1));
    client_ptr_->Register_OnData(std::bind(&MainWindow::OnData, this, std::placeholders::_1));
    client_ptr_->Register_OnTestData(std::bind(&MainWindow::OnTestData, this, std::placeholders::_1));

    /* Setup a timer that repeatedly calls MainWindow::realtimeDataSlot: */
    connect(&data_timer_, SIGNAL(timeout()), this, SLOT(realtimeDataSlot()));
    data_timer_.start(10); // Interval 0 means to client_ptr_->isConnected as fast as possible
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Flexiv Force Sensor GUI",
                                                               tr("Are you sure?\n"),
                                                               QMessageBox::Cancel | QMessageBox::Yes,
                                                               QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes)
    {
        event->ignore();
    }
    else
    {
        /* wait until client stop */
        while (false == client_ptr_->Stop())
            ;
        event->accept();
    }
}

void MainWindow::SetupGUI()
{
    /* setting panel */
    ui->lineEdit_target_device_ip->setText("192.168.0.5");

    /* info panel settings */
    ui->lineEdit_hall_1_mean->setEnabled(false);
    ui->lineEdit_hall_2_mean->setEnabled(false);
    ui->lineEdit_hall_3_mean->setEnabled(false);
    ui->lineEdit_hall_4_mean->setEnabled(false);
    ui->lineEdit_hall_1_std->setEnabled(false);
    ui->lineEdit_hall_2_std->setEnabled(false);
    ui->lineEdit_hall_3_std->setEnabled(false);
    ui->lineEdit_hall_4_std->setEnabled(false);

    /* demo hall read out */
    SetupHallPlot(ui->plot_hall_readout_1);
    SetupHallPlot(ui->plot_hall_readout_2);
    SetupHallPlot(ui->plot_hall_readout_3);
    SetupHallPlot(ui->plot_hall_readout_4);

    /* magnet qc test read out */
    SetupMagnetQCPlot(ui->plot_magnetQC_hall_readout_1);
    SetupMagnetQCPlot(ui->plot_magnetQC_hall_readout_2);
    SetupMagnetQCPlot(ui->plot_magnetQC_hall_readout_3);
    SetupMagnetQCPlot(ui->plot_magnetQC_hall_readout_4);

    /* calibration test */
    SetupCaliPlot(ui->plot_cali_hall_readout_1);
    SetupCaliPlot(ui->plot_cali_hall_readout_2);
    SetupCaliPlot(ui->plot_cali_hall_readout_3);
    SetupCaliPlot(ui->plot_cali_hall_readout_4);

    /* temperature test */
    SetupTempPlot(ui->plot_temp_hall_readout_1);
    SetupTempPlot(ui->plot_temp_hall_readout_2);
    SetupTempPlot(ui->plot_temp_hall_readout_3);
    SetupTempPlot(ui->plot_temp_hall_readout_4);

    /* setup demo */
    SetupDemo();

    ui->wsb_client->setCurrentIndex(0);
}

void MainWindow::SetupHallPlot(QCustomPlot *plot)
{
    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setColor(QColor(40, 110, 255));
    plot->addGraph();
    plot->graph(0)->setPen(pen);

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%s");
    plot->xAxis->setTicker(timeTicker);
    plot->xAxis->setLabel("Time (s)");
    plot->axisRect()->setupFullAxesBox();
    plot->yAxis->setRange(0, 65535);
    plot->yAxis->setLabel("Hall readout");

    /* make left and bottom axes transfer their ranges to right and top axes: */
    connect(plot->xAxis, SIGNAL(rangeChanged(QCPRange)), plot->xAxis2, SLOT(setRange(QCPRange)));
    connect(plot->yAxis, SIGNAL(rangeChanged(QCPRange)), plot->yAxis2, SLOT(setRange(QCPRange)));
}

void MainWindow::SetupDemo()
{
    QPen pen;
    pen.setStyle(Qt::SolidLine);
    pen.setColor(QColor(40, 110, 255));
    ui->plot_demo_force_reading->addGraph();
    ui->plot_demo_force_reading->graph(0)->setPen(pen);

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%s");
    ui->plot_demo_force_reading->xAxis->setTicker(timeTicker);
    ui->plot_demo_force_reading->xAxis->setLabel("Time (s)");
    ui->plot_demo_force_reading->axisRect()->setupFullAxesBox();
    ui->plot_demo_force_reading->yAxis->setRange(-40, 40);
    ui->plot_demo_force_reading->yAxis->setLabel("Reading");

    /* make left and bottom axes transfer their ranges to right and top axes: */
    connect(ui->plot_demo_force_reading->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->plot_demo_force_reading->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->plot_demo_force_reading->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->plot_demo_force_reading->yAxis2, SLOT(setRange(QCPRange)));
}

void MainWindow::UpdateHallPlot(QCustomPlot *plot, double time, int hall_id)
{
    plot->graph(0)->addData(time, hall_[hall_id - 1]);
    plot->xAxis->setRange(time, 5, Qt::AlignRight);
    plot->replot();
}

void MainWindow::UpdateDemo(double time)
{
    ui->plot_demo_force_reading->graph(0)->addData(time, test_fz_);
    ui->plot_demo_force_reading->xAxis->setRange(time, 20, Qt::AlignRight);
    ui->plot_demo_force_reading->replot();

    ui->label_demo_force_reading->setText("Force:   " + QString::number(fz_, 'f', 3));
}

void MainWindow::realtimeDataSlot()
{
    static size_t loop_count = 0;

    double test_time_s = (double)test_tick_ / 1000.0;
    double time_s = (double)tick_ / 1000.0;
    loop_count++;

    /* update sensor */
    UpdateHallPlot(ui->plot_hall_readout_1, test_time_s, 1);
    UpdateHallPlot(ui->plot_hall_readout_2, test_time_s, 2);
    UpdateHallPlot(ui->plot_hall_readout_3, test_time_s, 3);
    UpdateHallPlot(ui->plot_hall_readout_4, test_time_s, 4);

    /* */
    UpdateDemo(test_time_s);

    /* upgrade magnet qc graph */
    UpdateMagnetQCPlot(ui->plot_magnetQC_hall_readout_1, test_time_s, 1);
    UpdateMagnetQCPlot(ui->plot_magnetQC_hall_readout_2, test_time_s, 2);
    UpdateMagnetQCPlot(ui->plot_magnetQC_hall_readout_3, test_time_s, 3);
    UpdateMagnetQCPlot(ui->plot_magnetQC_hall_readout_4, test_time_s, 4);

    /* upgrade calibration graph */
    UpdateCaliPlot(ui->plot_cali_hall_readout_1, test_time_s, 1);
    UpdateCaliPlot(ui->plot_cali_hall_readout_2, test_time_s, 2);
    UpdateCaliPlot(ui->plot_cali_hall_readout_3, test_time_s, 3);
    UpdateCaliPlot(ui->plot_cali_hall_readout_4, test_time_s, 4);

    /* upgrade temperature graph*/
    UpdateTempPlot(ui->plot_temp_hall_readout_1, 1);
    UpdateTempPlot(ui->plot_temp_hall_readout_2, 2);
    UpdateTempPlot(ui->plot_temp_hall_readout_3, 3);
    UpdateTempPlot(ui->plot_temp_hall_readout_4, 4);

    /* refresh if recv request */
    if (true == client_ptr_->isConnected() && is_refresh_ == true)
    {
        /* version */
        std::string version = "v" + std::to_string(version_major_) + "." + std::to_string(version_minor_) + "." + std::to_string(version_patch_);
        ui->label_firmware_version->setText(QString::fromStdString(version));

        /* IP setting */
        std::string ip_ram;
        IPConvert2Str(ip_addr_, ip_ram);
        ui->lineEdit_ip->setText(QString::fromStdString(ip_ram));
        IPConvert2Str(ip_mask_, ip_ram);
        ui->lineEdit_net_mask->setText(QString::fromStdString(ip_ram));
        IPConvert2Str(ip_gw_, ip_ram);
        ui->lineEdit_gate_way->setText(QString::fromStdString(ip_ram));

        /* interval */
        ui->spinBox_data_interval->setValue(data_interval_);
        /* hall gain */
        ui->lineEdit_gain_1->setText(QString::fromStdString(std::to_string(gains_[0])));
        ui->lineEdit_gain_2->setText(QString::fromStdString(std::to_string(gains_[1])));
        ui->lineEdit_gain_3->setText(QString::fromStdString(std::to_string(gains_[2])));
        ui->lineEdit_gain_4->setText(QString::fromStdString(std::to_string(gains_[3])));
        /* hall offset */
        ui->lineEdit_gain_offset_1->setText(QString::fromStdString(std::to_string(gain_offsets_[0])));
        ui->lineEdit_gain_offset_2->setText(QString::fromStdString(std::to_string(gain_offsets_[1])));
        ui->lineEdit_gain_offset_3->setText(QString::fromStdString(std::to_string(gain_offsets_[2])));
        ui->lineEdit_gain_offset_4->setText(QString::fromStdString(std::to_string(gain_offsets_[3])));
        /* test interval */
        ui->spinBox_test_data_interval->setValue(test_data_interval_);

        is_refresh_ = false;
    }

    /* refresh every 200ms */
    if (loop_count % 20 == 0)
    {
        /* Update info in calibration panel */
        ui->label_info_hall_1->setText("Hall Channel 1: " + QString::number(hall_[0]));
        ui->label_info_hall_2->setText("Hall Channel 2: " + QString::number(hall_[1]));
        ui->label_info_hall_3->setText("Hall Channel 3: " + QString::number(hall_[2]));
        ui->label_info_hall_4->setText("Hall Channel 4: " + QString::number(hall_[3]));

        ui->label_info_ambient_temp->setText("Ambient temperature: " +
                                             QString::number(ambient_temperature_) + " °C");
        ui->label_info_imu_temp->setText("IMU temperature: " +
                                         QString::number(imu_temperature_) + " °C");

        /* Take a snapshot on pcb check info panel */
        ui->lineEdit_hall_1_mean->setText(QString::number(task_manage_.pcb_check_val_.hall_mean[0]));
        if (true == task_manage_.pcb_check_val_.mean_result[0])
            ui->lineEdit_hall_1_mean->setStyleSheet("font-size: 25px; height: 30px; background: green; color: black");
        else
            ui->lineEdit_hall_1_mean->setStyleSheet("font-size: 25px; height: 30px; background: red; color: black");

        ui->lineEdit_hall_1_std->setText(QString::number(task_manage_.pcb_check_val_.hall_std[0]));
        if (true == task_manage_.pcb_check_val_.std_result[0])
            ui->lineEdit_hall_1_std->setStyleSheet("font-size: 25px; height: 30px; background: green; color: black");
        else
            ui->lineEdit_hall_1_std->setStyleSheet("font-size: 25px; height: 30px; background: red; color: black");

        ui->lineEdit_hall_2_mean->setText(QString::number(task_manage_.pcb_check_val_.hall_mean[1]));
        if (true == task_manage_.pcb_check_val_.mean_result[1])
            ui->lineEdit_hall_2_mean->setStyleSheet("font-size: 25px; height: 30px; background: green; color: black");
        else
            ui->lineEdit_hall_2_mean->setStyleSheet("font-size: 25px; height: 30px; background: red; color: black");

        ui->lineEdit_hall_2_std->setText(QString::number(task_manage_.pcb_check_val_.hall_std[1]));
        if (true == task_manage_.pcb_check_val_.std_result[1])
            ui->lineEdit_hall_2_std->setStyleSheet("font-size: 25px; height: 30px; background: green; color: black");
        else
            ui->lineEdit_hall_2_std->setStyleSheet("font-size: 25px; height: 30px; background: red; color: black");

        ui->lineEdit_hall_3_mean->setText(QString::number(task_manage_.pcb_check_val_.hall_mean[2]));
        if (true == task_manage_.pcb_check_val_.mean_result[2])
            ui->lineEdit_hall_3_mean->setStyleSheet("font-size: 25px; height: 30px; background: green; color: black");
        else
            ui->lineEdit_hall_3_mean->setStyleSheet("font-size: 25px; height: 30px; background: red; color: black");

        ui->lineEdit_hall_3_std->setText(QString::number(task_manage_.pcb_check_val_.hall_std[2]));
        if (true == task_manage_.pcb_check_val_.std_result[2])
            ui->lineEdit_hall_3_std->setStyleSheet("font-size: 25px; height: 30px; background: green; color: black");
        else
            ui->lineEdit_hall_3_std->setStyleSheet("font-size: 25px; height: 30px; background: red; color: black");

        ui->lineEdit_hall_4_mean->setText(QString::number(task_manage_.pcb_check_val_.hall_mean[3]));
        if (true == task_manage_.pcb_check_val_.mean_result[3])
            ui->lineEdit_hall_4_mean->setStyleSheet("font-size: 25px; height: 30px; background: green; color: black");
        else
            ui->lineEdit_hall_4_mean->setStyleSheet("font-size: 25px; height: 30px; background: red; color: black");

        ui->lineEdit_hall_4_std->setText(QString::number(task_manage_.pcb_check_val_.hall_std[3]));
        if (true == task_manage_.pcb_check_val_.std_result[3])
            ui->lineEdit_hall_4_std->setStyleSheet("font-size: 25px; height: 30px; background: green; color: black");
        else
            ui->lineEdit_hall_4_std->setStyleSheet("font-size: 25px; height: 30px; background: red; color: black");

        /* temperature information panel */
        ui->label_temp_info_hall_1->setText("Hall Channel 1: " + QString::number(hall_[0]));
        ui->label_temp_info_hall_2->setText("Hall Channel 2: " + QString::number(hall_[1]));
        ui->label_temp_info_hall_3->setText("Hall Channel 3: " + QString::number(hall_[2]));
        ui->label_temp_info_hall_4->setText("Hall Channel 4: " + QString::number(hall_[3]));

        ui->label_temp_info_ambient_temp->setText("Ambient temperature: " + QString::number(ambient_temperature_) + " °C");
        ui->label_temp_info_imu_temp->setText("IMU temperature: " + QString::number(imu_temperature_) + " °C");

        /* set test message */
        ui->label_state_msg->setText(QString::fromStdString(task_manage_.message_));
    }
}

/* =========================  */
/* Private slots definitions  */
/* =========================  */
void MainWindow::on_lineEdit_ip_textChanged(const QString &arg1)
{
    ui->pushButton_ip_apply->setEnabled(true);
}

void MainWindow::on_pushButton_snapshot_clicked()
{
    task_manage_.StartTest(TaskManage::kPcbCheck);
}

void MainWindow::on_pushButton_start_client_clicked()
{
    std::string ip_address = ui->lineEdit_target_device_ip->text().toStdString();
    std::regex regex("(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])");
    /* check */
    if (!regex_match(ip_address, regex))
    {
        QMessageBox::warning(this, "Ip", "Invalid ip address");
        return;
    }

    client_ptr_->setAddress(Socket::Address::Ipv4(ip_address.c_str(), 1000));

    if (false == client_ptr_->Start())
    {
        qDebug() << "[App] Start fail";
        QMessageBox::warning(this, "connect", "Unable to connect to the server, Please check the device and PC ip");
        return;
    }

    std::chrono::system_clock::time_point start_time = std::chrono::system_clock::now();

    /* Wait to connect to the server */
    while (1)
    {
        if (true == client_ptr_->isConnected())
        {
            break;
        }

        std::chrono::system_clock::time_point current = std::chrono::system_clock::now();

        unsigned int time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(
                                        std::chrono::system_clock::now() - start_time)
                                        .count();
        /* wait 2 second */
        if (time_elapsed > 3000)
        {
            qDebug() << "[App] Start fail";
            QMessageBox::warning(this, "connect", "Unable to connect to the server, Please check the device and PC ip");
            return;
        }
    }

    /* refresh device information after connected */
    if (client_ptr_->isConnected() == true)
    {
        client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kSetInterval, 0);
        client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kSetTestInterval, 10);
        QThread::msleep(50);
        on_pushButton_refresh_clicked();
    }
}

void MainWindow::on_pushButton_stop_client_clicked()
{
    /* Stop */
    if (false == client_ptr_->Stop())
    {
        qDebug() << "Stop fail";
    }
}

void MainWindow::on_pushButton_ip_apply_clicked()
{
    if (client_ptr_->isConnected() == false)
    {
        qDebug() << "The Server is not connected";
        QMessageBox::warning(this, "Client", "The Server is not connected");
        return;
    }

    qDebug() << "[GUI] IP address set to: " << ui->lineEdit_ip->text();
    qDebug() << "[GUI] Net mask set to: " << ui->lineEdit_net_mask->text();
    qDebug() << "[GUI] Gate way set to: " << ui->lineEdit_gate_way->text();

    std::string ip_address_str = ui->lineEdit_ip->text().toStdString();
    std::string net_mask_str = ui->lineEdit_net_mask->text().toStdString();
    std::string gate_way_str = ui->lineEdit_gate_way->text().toStdString();

    std::regex regex("(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])");
    if (regex_match(ip_address_str, regex) && regex_match(net_mask_str, regex) && regex_match(gate_way_str, regex))
    {
    }
    else
    {
        QMessageBox::warning(this, "Ip", "Invalid ip address");
        return;
    }

    /* convert to in32 */
    uint32_t ip_addr, net_mask, gate_way;
    IPConvert(ip_address_str.c_str(), ip_addr);
    IPConvert(net_mask_str.c_str(), net_mask);
    IPConvert(gate_way_str.c_str(), gate_way);

    /* enable */
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kIP4, ip_addr, net_mask, gate_way);
}

void MainWindow::on_pushButton_ip_default_clicked()
{
    qDebug()
        << "[GUI] Reset IP address.";

    ui->lineEdit_ip->setText("192.168.0.5");
    ui->lineEdit_net_mask->setText("255.255.255.0");
    ui->lineEdit_gate_way->setText("192.168.0.1");

    ui->pushButton_ip_apply->setEnabled(true);
}

void MainWindow::on_pushButton_sensor_param_apply_clicked()
{
    if (client_ptr_->isConnected() == false)
    {
        qDebug() << "The Server is not connected";
        QMessageBox::warning(this, "Client", "The Server is not connected");
        return;
    }

    gains_[0] = ui->lineEdit_gain_1->text().toDouble();
    gains_[1] = ui->lineEdit_gain_2->text().toDouble();
    gains_[2] = ui->lineEdit_gain_3->text().toDouble();
    gains_[3] = ui->lineEdit_gain_4->text().toDouble();

    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kSetHallGain,
                            gains_[0] * kHallGainFactor, gains_[1] * kHallGainFactor, gains_[2] * kHallGainFactor, gains_[3] * kHallGainFactor);
}

void MainWindow::on_pushButton_reboot_device_clicked()
{
    qDebug() << "[GUI] reboot device";
    if (client_ptr_->isConnected() == false)
    {
        qDebug() << "The Server is not connected";
        QMessageBox::warning(this, "Client", "The Server is not connected");
        return;
    }

    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kReboot);
}

void MainWindow::on_pushButton_refresh_clicked()
{
    qDebug() << "[GUI] Refresh device information";
    if (client_ptr_->isConnected() == false)
    {
        qDebug() << "The Server is not connected";
        QMessageBox::warning(this, "Client", "The Server is not connected");
        return;
    }

    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kFaultCode);
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kVersion);
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kIP4);
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kInterval);
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kTestInterval);
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kAccelerometer);
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kGyroscope);
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kHallGain);
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kHallOffset);
}

void MainWindow::on_pushButton_publish_interval_clicked()
{
    qDebug() << "[GUI] Setting interval";

    if (client_ptr_->isConnected() == false)
    {
        qDebug() << "The Server is not connected";
        QMessageBox::warning(this, "Client", "The Server is not connected");
        return;
    }

    int32_t test_data_interval = ui->spinBox_test_data_interval->value();
    int32_t data_interval = ui->spinBox_data_interval->value();

    /* interval greater than 5 or equal to 0 */
    test_data_interval = (test_data_interval > 5 || test_data_interval == 0) ? test_data_interval : 5;
    data_interval = (data_interval > 5 || data_interval == 0) ? data_interval : 5;

    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kSetTestInterval, test_data_interval);
    client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kSetInterval, data_interval);
}

void MainWindow::on_pushButton_upgrade_firmware_clicked()
{
    qDebug() << "[GUI] Start upgrade firmware, no support";
    if (client_ptr_->isConnected() == false)
    {
        qDebug() << "The Server is not connected";
        QMessageBox::warning(this, "Client", "The Server is not connected");
        return;
    }

    QString file_name = ui->lineEdit_firmware_file->text();
    qDebug() << "upgrade file ; " << file_name;

    QFile file(file_name);
    if (file.exists())
    {
        qDebug() << "Upgrade file: " << file_name;
        /* Stop publish data before start upgrade */
        client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kTestInterval, 0);
        client_ptr_->SendSystem(SYSTEM__CMD_TYPE__kInterval, 0);

        TinyFtpTransfer_SendFile(client_ptr_.get(), file_name.toStdString().c_str());
    }
    else
        QMessageBox::warning(this, "Upgrade", "The file does not exist");
}

void MainWindow::on_pushButton_select_firmware_clicked()
{
    qDebug() << "[GUI] Select firmware";
    if (client_ptr_->isConnected() == false)
    {
        qDebug() << "The Server is not connected";
        QMessageBox::warning(this, "Client", "The Server is not connected");
        return;
    }

    QString fileName = QFileDialog::getOpenFileName(this, tr("Upgrade files"), "./", tr("Files (*.bin *.flexiv)"));

    QFile file(fileName);

    if (file.exists())
    {
        ui->lineEdit_firmware_file->setText(fileName);
    }
    else
    {
        qDebug() << "The file does not exist";
        QMessageBox::warning(this, "Upgrade", "The file does not exist");
    }
}

void MainWindow::on_pushButton_start_magnetQC_clicked()
{
    qDebug() << "[GUI] start_magnetQC";
    task_manage_.StartTest(TaskManage::kMagnetQc);
}

void MainWindow::on_pushButton_finish_magnetQC_clicked()
{
    qDebug() << "[GUI] finish_magnetQC";
    task_manage_.StopTest();
}

void MainWindow::on_pushButton_start_temp_test_clicked()
{
    qDebug() << "[GUI] start_temp_test";
    task_manage_.StartTest(TaskManage::kTemperature);
}

void MainWindow::on_pushButton_finish_temp_test_clicked()
{
    qDebug() << "[GUI] finish_temp_test";
    task_manage_.StopTest();
}

void MainWindow::on_pushButton_start_calibration_clicked()
{
    qDebug() << "[GUI] start_calibration_clicked";
    task_manage_.calibration_arg_.sub_text_name = ui->comboBox_cali_status->currentText().toStdString();
    task_manage_.StartTest(TaskManage::kHallCalibration);
}

void MainWindow::on_pushButton_calibration_take_data_clicked()
{
    qDebug() << "[GUI] pushButton_calibration_takeData_clicked";
    task_manage_.calibration_arg_.take_data_trigger = true;
}

void MainWindow::SetupMagnetQCPlot(QCustomPlot *plot)
{
    plot->addGraph();
    plot->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
    plot->graph(0)->setLineStyle(QCPGraph::lsNone); // No line between points

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%s");
    plot->xAxis->setTicker(timeTicker);
    plot->xAxis->setLabel("Time (s)");
    plot->yAxis->setRange(0, 65535);
    plot->yAxis->setLabel("Reading");
}

void MainWindow::SetupCaliPlot(QCustomPlot *plot)
{
    plot->addGraph();
    plot->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
    plot->graph(0)->setLineStyle(QCPGraph::lsNone); // No line between points

    QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
    timeTicker->setTimeFormat("%s");
    plot->xAxis->setTicker(timeTicker);
    plot->xAxis->setLabel("Time (s)");
    plot->yAxis->setRange(0, 65535);
    plot->yAxis->setLabel("Reading");
}

void MainWindow::SetupTempPlot(QCustomPlot *plot)
{
    plot->addGraph();
    plot->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
    plot->graph(0)->setLineStyle(QCPGraph::lsNone); // No line between points

    plot->xAxis->setRange(-50, 150);
    plot->xAxis->setLabel("Temperature °C");
    plot->yAxis->setRange(0, 65535);
    plot->yAxis->setLabel("Reading");
}

void MainWindow::UpdateMagnetQCPlot(QCustomPlot *plot, double time_s, int hall_id)
{
    plot->graph(0)->addData(time_s, hall_[hall_id - 1]);
    plot->xAxis->setRange(time_s, 5, Qt::AlignRight);
    plot->replot();
}

void MainWindow::UpdateCaliPlot(QCustomPlot *plot, double time_s, int hall_id)
{
    plot->graph(0)->addData(time_s, hall_[hall_id - 1]);
    plot->xAxis->setRange(time_s, 5, Qt::AlignRight);
    plot->replot();
}

void MainWindow::UpdateTempPlot(QCustomPlot *plot, int hall_id)
{
    plot->graph(0)->addData(ambient_temperature_, hall_[hall_id - 1]);
    plot->replot();
}

void MainWindow::SetSerialNumber(char *sn)
{
    task_manage_.serial_number_ = sn;
    serial_number_ = sn;

    setWindowTitle(QString("F01A GUI - ") + QString::fromStdString(FLEXIV_F01A_SOFTWARE_VERSION) + QString(" - ") + QString::fromStdString(serial_number_));
}

void MainWindow::OnSystem(_System *system)
{
    /* Print */
    qDebug()
        << "cmd type : " << system->cmd << ", params<" << system->param_1 << "," << system->param_2 << "," << system->param_3 << "," << system->param_4 << "," << system->param_5 << ">";

    switch (system->cmd)
    {
    case SYSTEM__CMD_TYPE__kVersion:
    {
        version_major_ = system->param_1;
        version_minor_ = system->param_2;
        version_patch_ = system->param_3;
        is_refresh_ = true;
        break;
    }
    case SYSTEM__CMD_TYPE__kIP4:
    {
        ip_addr_ = system->param_1;
        ip_mask_ = system->param_2;
        ip_gw_ = system->param_3;
        break;
    }
    case SYSTEM__CMD_TYPE__kInterval:
    {
        data_interval_ = system->param_1;
        break;
    }
    case SYSTEM__CMD_TYPE__kAccelerometer:
    {
        break;
    }
    case SYSTEM__CMD_TYPE__kHallGain:
    {
        gains_[0] = (float)system->param_1 / kHallGainFactor;
        gains_[1] = (float)system->param_2 / kHallGainFactor;
        gains_[2] = (float)system->param_3 / kHallGainFactor;
        gains_[3] = (float)system->param_4 / kHallGainFactor;
        break;
    }
    case SYSTEM__CMD_TYPE__kHallOffset:
    {
        gain_offsets_[0] = system->param_1;
        gain_offsets_[1] = system->param_2;
        gain_offsets_[2] = system->param_3;
        gain_offsets_[3] = system->param_4;
        break;
    }
    case SYSTEM__CMD_TYPE__kTestInterval:
    {
        test_data_interval_ = system->param_1;
        break;
    }
    case SYSTEM__CMD_TYPE__kGyroscope:
    {
        break;
    }
    default:
        break;
    }
}

void MainWindow::OnData(_Sensor *sensor)
{
    /* copy data to ui */
    tick_ = sensor->tick;
    fz_ = sensor->fz;
}

void MainWindow::OnTestData(_TestSensor *test_sensor)
{
    /* copy data to ui */
    test_tick_ = test_sensor->tick;
    hall_[0] = test_sensor->hall_a;
    hall_[1] = test_sensor->hall_b;
    hall_[2] = test_sensor->hall_c;
    hall_[3] = test_sensor->hall_d;
    ambient_temperature_ = test_sensor->ambient_temperature;
    imu_temperature_ = test_sensor->pcb_temperature;
    test_fz_ = test_sensor->fz;

    /* Run task manage */
    task_manage_.Run(test_sensor);
}
