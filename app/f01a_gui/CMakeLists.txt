cmake_minimum_required(VERSION 3.1.3)

project(f01a_gui)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

# print compile info
set(CMAKE_VERBOSE_MAKEFILE ON)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/../../bin")

# qt components
find_package(Qt5 COMPONENTS Core Gui Widgets PrintSupport REQUIRED)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}/include
    ${CMAKE_CURRENT_SOURCE_DIR}/build
)

# Add source files
file(GLOB SRC_FILES 
	${CMAKE_CURRENT_SOURCE_DIR}/src/*.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/src/task/*.cc
    ${CMAKE_CURRENT_SOURCE_DIR}/*.cc
	)

file(GLOB QT_UI_FILES ${CMAKE_CURRENT_SOURCE_DIR}/*.ui)
qt5_wrap_ui(QT_UI_SOURCE_FILES ${QT_UI_FILES})

add_executable(${PROJECT_NAME}
    ${QT_UI_SOURCE_FILES}
    ${SRC_FILES}
)

set(flexiv_f01a_software_DIR 
        ${CMAKE_CURRENT_SOURCE_DIR}/../../install/lib/cmake/flexiv_f01a_software)
find_package(flexiv_f01a_software REQUIRED)

target_link_libraries(${PROJECT_NAME} 
    PUBLIC
    flexiv_f01a_software
    Qt5::Core 
    Qt5::Gui 
    Qt5::Widgets 
    Qt5::PrintSupport
)

target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_14)
set_target_properties(${PROJECT_NAME} PROPERTIES CXX_EXTENSIONS OFF)
target_compile_options(${PROJECT_NAME} PRIVATE -Werror)