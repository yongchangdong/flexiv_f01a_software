/**
 * @file mainwindow.h
 * @copyright Flexiv Robotics 2022
 * @brief F01A application GUI
 * @author Jinzhao
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include <memory>
#include <string>
#include "qcustomplot.h"
#include <wsb_client.h>
#include "task_manage.h"

/*
 * Forward declaration: MainWindow
 */
namespace Ui
{
    class MainWindow;
} // namespace Ui

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Main windows for f01a UI instance
     * @param[in] parent QT parent
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /**
     * @brief Set serial number
     * @param[in] sn Serial number
     */
    void SetSerialNumber(char *sn);

protected:
    /**
     * @brief Setup GUI object
     */
    void SetupGUI();

    /**
     * @brief Setup hall plot
     * @param[in] plot Plot instance
     */
    void SetupHallPlot(QCustomPlot *plot);

    /**
     * @brief Setup magnet QC test plot
     * @param[in] plot Plot instance
     */
    void SetupMagnetQCPlot(QCustomPlot *plot);

    /**
     * @brief Setup magnet QC plot
     * @param[in] plot Plot instance
     */
    void SetupCaliPlot(QCustomPlot *plot);

    /**
     * @brief Setup temperature plot
     * @param[in] plot Plot instance
     */
    void SetupTempPlot(QCustomPlot *plot);

    /**
     * @brief Setup demo plot
     */
    void SetupDemo();

    /**
     * @brief Update hall plot
     * @param[in] plot Plot instance
     * @param[in] time X-Axis time value in second
     * @param[in] hall_id Hall channel 0~3
     */
    void UpdateHallPlot(QCustomPlot *plot, double time, int hall_id);

    /**
     * @brief Update demo plot
     * @param[in] time X-Axis time value in second
     */
    void UpdateDemo(double time);

    /**
     * @brief Update magnet qc plot
     * @param[in] plot Plot instance
     * @param[in] time X-Axis time value in second
     * @param[in] hall_id Hall channel 0~3
     */
    void UpdateMagnetQCPlot(QCustomPlot *plot, double time_s, int hall_id);

    /**
     * @brief Update hall plot
     * @param[in] plot Plot instance
     * @param[in] time X-Axis time value in second
     * @param[in] hall_id Hall channel 0~3
     */
    void UpdateCaliPlot(QCustomPlot *plot, double time_s, int hall_id);

    /**
     * @brief Update temperature plot
     * @param[in] plot Plot instance
     * @param[in] hall_id Hall channel 0~3
     */
    void UpdateTempPlot(QCustomPlot *plot, int hall_id);

    /**
     * @brief Inherit from qt main windows class
     * @param[in] event Close event
     */
    void closeEvent(QCloseEvent *event);

private slots:
    void realtimeDataSlot();

    void on_lineEdit_ip_textChanged(const QString &arg1);

    void on_pushButton_snapshot_clicked();

    void on_pushButton_start_client_clicked();

    void on_pushButton_stop_client_clicked();

    void on_pushButton_ip_apply_clicked();

    void on_pushButton_ip_default_clicked();

    void on_pushButton_sensor_param_apply_clicked();

    void on_pushButton_reboot_device_clicked();

    void on_pushButton_refresh_clicked();

    void on_pushButton_publish_interval_clicked();

    void on_pushButton_upgrade_firmware_clicked();

    void on_pushButton_select_firmware_clicked();

    void on_pushButton_start_magnetQC_clicked();

    void on_pushButton_finish_magnetQC_clicked();

    void on_pushButton_start_temp_test_clicked();

    void on_pushButton_finish_temp_test_clicked();

    void on_pushButton_start_calibration_clicked();

    void on_pushButton_calibration_take_data_clicked();

private:
    /**
     * @brief System data receive callback. This function
     * registers with the wsb client thread.
     * @param[in] data System data
     */
    void OnSystem(_System *data);

    /**
     * @brief Sensor data receive callback. This function
     * registers with the wsb client thread.
     * @param[in] data Sensor data
     */
    void OnData(_Sensor *data);

    /**
     * @brief Sensor Test data receive callback. This function
     * registers with the wsb client thread.
     * @param[in] data Sensor test data
     */
    void OnTestData(_TestSensor *data);

    Ui::MainWindow *ui;

    QTimer data_timer_;

    /* wsb client instance */
    std::shared_ptr<WsbClient> client_ptr_;

    /* serial number, it's used for generation log file*/
    std::string serial_number_;

    /* Whether to request a refresh of device setup information */
    bool is_refresh_;

    /* device setting */
    int32_t ip_addr_;
    int32_t ip_mask_;
    int32_t ip_gw_;
    int32_t test_data_interval_;
    int32_t data_interval_;
    uint32_t version_major_;
    uint32_t version_minor_;
    uint32_t version_patch_;
    float gains_[4] = {0.005, 0.005, 0.005, 0.005};
    int gain_offsets_[4] = {32767};

    /* sensor data */
    uint32_t tick_;
    float fz_;

    /* test data */
    uint32_t test_tick_;
    int32_t hall_[4];
    float ambient_temperature_;
    float imu_temperature_;
    float test_fz_;

    /* task machnine manage  */
    TaskManage task_manage_;
};

#endif // MAINWINDOW_H
