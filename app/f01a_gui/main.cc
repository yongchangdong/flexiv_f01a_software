#include "mainwindow.h"
#include <QApplication>
#include <log.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    TestLogger::Instance().Init();
    /* create lower-priority threads and set its function pointer */
    std::thread bg_task_thread(&Logger::Run, &TestLogger::Instance());

    MainWindow w;

    /* set serial number */
    if (argc > 1)
    {
        w.SetSerialNumber(argv[1]);
    }

    w.show();

    /* GUI main loop */
    a.exec();

    /* quit logger thread */
    TestLogger::Instance().Stop();
    bg_task_thread.join();
    return 0;
}
