/**
 * @file shared_queue.h
 * @copyright Copyright (c) Flexiv Robotics 2021
 *
 * @author Yongchang Dong
 */
#ifndef SHARED_QUEUE_H_
#define SHARED_QUEUE_H_

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <queue>

template <typename T> class SharedQueue
{
public:
    SharedQueue();
    ~SharedQueue();

    T& Front();
    void PopFront();

    void PushBack(const T& item);

    int Size();

private:
    std::deque<T> queue_;
    std::mutex mutex_;
    std::condition_variable cond_;
};

template <typename T> SharedQueue<T>::SharedQueue() { }

template <typename T> SharedQueue<T>::~SharedQueue() { }

template <typename T> T& SharedQueue<T>::Front()
{
    std::unique_lock<std::mutex> mlock(mutex_);
    while (queue_.empty()) {
        cond_.wait_for(mlock, std::chrono::milliseconds(100));
    }
    return queue_.front();
}

template <typename T> void SharedQueue<T>::PopFront()
{
    std::unique_lock<std::mutex> mlock(mutex_);
    while (queue_.empty()) {
        cond_.wait_for(mlock, std::chrono::milliseconds(100));
    }
    queue_.pop_front();
}

template <typename T> void SharedQueue<T>::PushBack(const T& item)
{
    std::unique_lock<std::mutex> mlock(mutex_);
    queue_.push_back(item);
    mlock.unlock();
    cond_.notify_one();
}

template <typename T> int SharedQueue<T>::Size()
{
    std::unique_lock<std::mutex> mlock(mutex_);
    int size = queue_.size();
    mlock.unlock();
    return size;
}

#endif /* SHARED_QUEUE_H_ */