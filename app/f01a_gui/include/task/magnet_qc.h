#ifndef MAGNET_QC_TASK_
#define MAGNET_QC_TASK_

#include "task_base.h"

class MagnetQC : public TaskBase
{
public:
  MagnetQC(TaskManage *manage) : TaskBase(manage){};
  ~MagnetQC() = default;

  RetCode Run(_TestSensor *test_data) override;
  RetCode Enter() override;
  RetCode Exit() override;

private:
};

#endif