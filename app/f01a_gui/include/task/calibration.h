#ifndef CALIBRATION_
#define CALIBRATION_

#include "task_base.h"

class Calibration : public TaskBase
{
public:
  Calibration(TaskManage *manage) : TaskBase(manage){};
  ~Calibration() = default;

  RetCode Run(_TestSensor *test_data) override;
  RetCode Enter() override;
  RetCode Exit() override;

private:
  int sample_count_ = 0;
  bool request_sample = false;
};

#endif