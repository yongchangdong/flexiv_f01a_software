#ifndef PCB_CHECK_H_
#define PCB_CHECK_H_

#include "task_base.h"
#include <vector>

class PcbCheck : public TaskBase
{
public:
  PcbCheck(TaskManage *manage) : TaskBase(manage){};
  ~PcbCheck() = default;

  RetCode Run(_TestSensor *test_data) override;
  RetCode Enter() override;
  RetCode Exit() override;

private:
  std::vector<int> hall_[4];
};

#endif