#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_

#include "task_base.h"

class Temperature : public TaskBase
{
public:
  Temperature(TaskManage *manage) : TaskBase(manage){};
  ~Temperature() = default;

  RetCode Run(_TestSensor *test_data) override;
  RetCode Enter() override;
  RetCode Exit() override;

private:
  int sample_count_ = 0;
};

#endif