/**
 * @file task_base.h
 * @copyright Copyright (c) Flexiv Robotics 2022
 *
 * @author YongChang Dong
 */
#ifndef TASK_BASE_H_
#define TASK_BASE_H_

#include <iostream>
#include "logger.h"
#include <wsb_f.pb-c.h>
#include "database.h"
#include <string>

class TaskManage;

class TaskBase
{
public:
    TaskBase();
    TaskBase(TaskManage *manage);
    ~TaskBase();
    /** Task State */
    enum RetCode
    {
        kNoErr,
        kDone,
        kStart,
        kError,
    };

    /**
     * @brief Perform the main test tasks
     * @return kNoErr if run not error
     *         kDone if task complete
     *         kError if error occur
     */
    virtual RetCode Run(_TestSensor *test_data) = 0;

    /**
     * @brief Task entry function for parameter loading and resource allocation
     * @return kNoErr if task init success, task will periodically execute run()
     *         kError if init fail,task work will stop
     */
    virtual RetCode Enter();

    /**
     * @brief Exits the task once everything is completed or the user aborts
     */
    virtual RetCode Exit();

    void Message(std::string msg);

protected:
    /** Variables for logger */
    DatabaseData data_;

    /** Loop counter that ticks every control cycle */
    uint32_t loop_counter_ = 0;

    TaskManage *task_manage_;
};

#endif /* TASK_BASE_H_ */