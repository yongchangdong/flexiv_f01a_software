/**
 * @file log.h
 * @copyright Flexiv Robotics 2021
 *
 * @author Yongchang Dong
 */

#ifndef LOG_H_
#define LOG_H_

#include <spdlog/sinks/basic_file_sink.h>
#include <iostream>
#include <string>
#include <atomic>
#include "data_singleton.h"
#include "database.h"
#include "shared_queue.h"

/**
 * @class Logger
 * @brief Run function for logging
 */
class Logger
{
public:
    Logger();
    virtual ~Logger();

    /**
     * @brief Initialize the logging thread
     */
    void Init(void);

    /**
     * @brief Run the logging loop
     */
    void Run(void);

    /**
     * @brief Stop logging loop
     */
    void Stop(void);

    /**
     * @brief Set close file atomic boolean to be true for the logging thread to
     * close the file
     */
    void SetCloseFile(void);

    /**
     * @brief Set create file atomic boolean to be true for the logging thread
     * to create the file
     * @param[in] file_name File name
     * @param[in] file_header The first header line
     */
    void SetCreateFile(const std::string &file_name, const std::vector<std::string> &file_header);

    /**
     * @brief Copy data into the queue (pass by const reference)
     * @param[in] data Joint test data to be recorded
     */
    void CopyDataToQueue(const DatabaseData &data);

private:
    /** Logger for test results */
    std::shared_ptr<spdlog::logger> test_logger_ = nullptr;

    /** Atomic bool to indicate the logger is running */
    std::atomic_bool run_;

    /** Atomic bool to request a file close operation */
    std::atomic_bool close_file_;

    /** Atomic bool to request a file open operation */
    std::atomic_bool create_file_;

    /** Shared queue to buffer data to be logged */
    SharedQueue<DatabaseData> queue_;

    /** Name of file for logger to record to */
    std::string file_name_;

    /** Header string to indicate data types and names */
    std::vector<std::string> file_header_;

    /**
     * @brief Create logging file, only used internally (private) by the logging
     * thread
     */
    void CreateFile(void);

    /**
     * @brief Close logging file, only used internally (private) by the logging
     * thread
     */
    void CloseFile(void);
};

typedef S<Logger> TestLogger;

#endif /* LOGGER_H_ */