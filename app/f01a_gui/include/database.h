/**
 * @file database.h
 * @copyright Copyright (c) Flexiv Robotics 2021
 *
 * @author Xiyang Yeh
 */
#ifndef DATABASE_H_
#define DATABASE_H_

#include <map>

typedef std::map<std::string, float> DatabaseData;
typedef std::pair<std::string, float> OneDataPoint;

#endif /* DATABASE_H_ */
