/**
 * @file data_singleton.h
 * @copyright Copyright (c) Flexiv Robotics 2021
 *
 * @author Xiyang Yeh
 */
#ifndef DATA_SINGLETON_H_
#define DATA_SINGLETON_H_

template <class T> class S
{
public:
    static T& Instance()
    {
        static T instance;

        return instance;
    }

private:
    S() { }
    ~S() { }

public:
    S(S const&) = delete;
    void operator=(S const&) = delete;
};

#endif // DATA_SINGLETON_H_
