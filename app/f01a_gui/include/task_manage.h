#ifndef TASK_MANAGE_H_
#define TASK_MANAGE_H_

#include <map>
#include "task/task_base.h"

class TaskManage
{
public:
  TaskManage();
  ~TaskManage() = default;

  enum TaskName
  {
    kNone,
    kHallCalibration,
    kPcbCheck,
    kMagnetQc,
    kTemperature,
  };

  enum State
  {
    kInit = 0,
    kStartup = 1,
    kRunTask = 2,
    kWaitUserInput = 3,
    kEnterTask = 4,
    kExitTask = 5,
  };

  // clang-format off
    const std::map<State, std::string> kStateName = {
        {kInit, "Init"},
        {kStartup, "Startup"},
        {kRunTask, "RunTask"},
        {kWaitUserInput, "WaitUserInput"},
        {kEnterTask,"kEnterTask"},
        {kExitTask,"kExitTask"},
    };
  // clang-format on

  /**
   * @brief Start test task
   * @param[in] task_name task dname
   */
  void StartTest(TaskName task_name);

  /**
   * @brief Stop task
   */
  void StopTest();

  /**
   * @brief Poll test state machine
   * @param[in] test_data  test data
   */
  void Run(_TestSensor *test_data);

  /**
   * @brief Calibration test value
   */
  struct CalibrationVal
  {
    std::string sub_text_name = "";
    bool take_data_trigger = false;
  } calibration_arg_;

  /**
   * @brief PCB check test value
   */
  struct PcbCheckVal
  {
    int hall_mean[4];
    bool mean_result[4]; ///< True if hall average is good. The GUI sets a green background.
    float hall_std[4];
    bool std_result[4]; ///< True if hall std is good. The GUI sets a green background.
  } pcb_check_val_;

  /* serial number */
  std::string serial_number_ = "";

  /* this message will print  to GUI  */
  std::string message_ = "";

private:
  State app_state_ = kInit;
  State last_app_state_ = kInit;

  std::unique_ptr<TaskBase> task_;

  TaskName task_name_;

  bool is_request_stop_ = false;
};

#endif