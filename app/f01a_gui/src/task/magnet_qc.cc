#include "task/magnet_qc.h"
#include "task_manage.h"

/** sample count  */
constexpr int kSampleCount = 500;

TaskBase::RetCode MagnetQC::Run(_TestSensor *test_data)
{
  RetCode ret = RetCode::kNoErr;

  loop_counter_++;

  /* add split log */
  data_["hall_1"] = test_data->hall_a;
  data_["hall_2"] = test_data->hall_b;
  data_["hall_3"] = test_data->hall_c;
  data_["hall_4"] = test_data->hall_d;
  data_["ambient_temp"] = test_data->ambient_temperature;
  data_["imu_temp"] = test_data->pcb_temperature;
  TestLogger::Instance().CopyDataToQueue(data_);

  Message("采样中,总共 :" + std::to_string(loop_counter_));

  if (loop_counter_ > kSampleCount)
    ret = RetCode::kDone;

  return ret;
}

/**
 * @brief Task entry function for parameter loading and resource allocation
 * @return kNoErr if task init success, task will periodically execute run()
 *         kError if init fail,task work will stop
 */
TaskBase::RetCode MagnetQC::Enter()
{
  loop_counter_ = 0;
  /* Initialize log file */
  std::string filename = "f01a-magnetqc-" + task_manage_->serial_number_ + ".csv";
  std::vector<std::string> header{
      "hall_1", "hall_2", "hall_3", "hall_4", "ambient_temp", "imu_temp"};
  TestLogger::Instance().SetCreateFile(filename, header);

  return RetCode::kDone;
}

/**
 * @brief Exits the task once everything is completed or the user aborts
 */
TaskBase::RetCode MagnetQC::Exit()
{
  TestLogger::Instance().SetCloseFile();
  return RetCode::kDone;
}