#include "task/pcb_check.h"
#include "task_manage.h"
#include "log.h"

/** hall mean value */
constexpr int kHallMean = 32767;
/** hall threshold */
constexpr int kHallMeanThreshold = 2000;
/** hall std value */
constexpr int kHallStd = 3;
/** hall std threshold */
constexpr int kHallStdThreshold = 2;

/* ================= */
/* Utility functions */
/* ================= */
float ComputeMean(std::vector<int> numbers)
{
  if (numbers.empty())
    return 0;

  float total = 0;
  for (int number : numbers)
  {
    total += number;
  }

  return (total / numbers.size());
}

float ComputeVariance(float mean, std::vector<int> numbers)
{
  if (numbers.empty())
    return 0;

  float result = 0;
  for (int number : numbers)
  {
    result += (number - mean) * (number - mean);
  }

  return sqrt(result / numbers.size());
}

TaskBase::RetCode PcbCheck::Run(_TestSensor *test_data)
{
  RetCode ret = RetCode::kNoErr;

  loop_counter_++;

  /* record log */
  if (loop_counter_ < 20)
  {
    hall_[0].push_back(test_data->hall_a);
    hall_[1].push_back(test_data->hall_b);
    hall_[2].push_back(test_data->hall_c);
    hall_[3].push_back(test_data->hall_d);

    loop_counter_ += 1;
  }
  else
  {
    /* calc mean sand std, and check result*/
    for (int i = 0; i < 4; i++)
    {
      /* mean */
      int hall_mean = ComputeMean(hall_[i]);
      task_manage_->pcb_check_val_.hall_mean[i] = hall_mean;

      if (abs(hall_mean - kHallMean) > kHallMeanThreshold)
      {
        task_manage_->pcb_check_val_.mean_result[i] = false;
      }
      else
      {
        task_manage_->pcb_check_val_.mean_result[i] = true;
      }

      /* std */
      int hall_std = ComputeVariance(task_manage_->pcb_check_val_.hall_mean[i], hall_[i]);
      task_manage_->pcb_check_val_.hall_std[i] = hall_std;

      if (abs(hall_std - kHallStd) > kHallStdThreshold)
      {
        task_manage_->pcb_check_val_.std_result[i] = false;
      }
      else
      {
        task_manage_->pcb_check_val_.std_result[i] = true;
      }
    }

    ret = RetCode::kDone;
  }

  return ret;
}

/**
 * @brief Task entry function for parameter loading and resource allocation
 * @return kNoErr if task init success, task will periodically execute run()
 *         kError if init fail,task work will stop
 */
TaskBase::RetCode PcbCheck::Enter()
{
  loop_counter_ = 0;

  hall_[0].clear();
  hall_[0].clear();
  hall_[0].clear();
  hall_[0].clear();

  return RetCode::kDone;
}

/**
 * @brief Exits the task once everything is completed or the user aborts
 */
TaskBase::RetCode PcbCheck::Exit()
{

  return RetCode::kDone;
}