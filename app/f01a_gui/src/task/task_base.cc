/**
 * @file task_base.cc
 * @copyright Copyright (c) Flexiv Robotics 2022
 *
 * @author YongChang Dong
 */
#include "task/task_base.h"
#include "task_manage.h"

TaskBase::TaskBase() { task_manage_ = nullptr; }

TaskBase::TaskBase(TaskManage *manage) { task_manage_ = manage; }

TaskBase::~TaskBase()
{
}

TaskBase::RetCode TaskBase::Enter()
{
  return RetCode::kDone;
}

TaskBase::RetCode TaskBase::Exit()
{
  return RetCode::kDone;
};

void TaskBase::Message(std::string msg)
{
  task_manage_->message_ = msg;
}