#include "task/temperature.h"
#include "task_manage.h"
#include "log.h"

/** sample count  */
constexpr int kSampleCountPerGroup = 5;
/** sample count  */
constexpr int kSampleCount = 7500;

TaskBase::RetCode Temperature::Run(_TestSensor *test_data)
{
  RetCode ret = RetCode::kNoErr;

  loop_counter_++;

  // Print 50 data with -1 as spliter to file
  if (loop_counter_ < kSampleCountPerGroup)
  {
    /* add split log */
    data_["hall_1"] = test_data->hall_a;
    data_["hall_2"] = test_data->hall_b;
    data_["hall_3"] = test_data->hall_c;
    data_["hall_4"] = test_data->hall_d;
    data_["ambient_temp"] = test_data->ambient_temperature;
    data_["imu_temp"] = test_data->pcb_temperature;
    TestLogger::Instance().CopyDataToQueue(data_);
  }
  else
  {
    /* add split for data processor */
    data_["hall_1"] = -1;
    data_["hall_2"] = -1;
    data_["hall_3"] = -1;
    data_["hall_4"] = -1;
    data_["ambient_temp"] = -1;
    data_["imu_temp"] = -1;

    TestLogger::Instance().CopyDataToQueue(data_);

    /* get enough data */
    if (0 == --sample_count_)
    {
      ret = RetCode::kDone;
    }

    /* clear */
    loop_counter_ = 0;
    logger::debug() << "Calibration sample : " << sample_count_;
    Message("采样中,还有" + std::to_string(sample_count_) + "次");
  }

  return ret;
}

/**
 * @brief Task entry function for parameter loading and resource allocation
 * @return kNoErr if task init success, task will periodically execute run()
 *         kError if init fail,task work will stop
 */
TaskBase::RetCode Temperature::Enter()
{
  loop_counter_ = 0;
  sample_count_ = kSampleCount;
  /* Initialize log file */
  std::string filename = "f01a-temp-comp-force_" + task_manage_->serial_number_ + ".csv";
  std::vector<std::string> header{
      "hall_1", "hall_2", "hall_3", "hall_4", "ambient_temp", "imu_temp"};
  TestLogger::Instance().SetCreateFile(filename, header);

  logger::debug() << "calibration create file " << filename;

  return RetCode::kDone;
}

/**
 * @brief Exits the task once everything is completed or the user aborts
 */
TaskBase::RetCode Temperature::Exit()
{
  TestLogger::Instance().SetCloseFile();
  return RetCode::kDone;
}