#include "task_manage.h"
#include <log.h>
#include "task/calibration.h"
#include "task/magnet_qc.h"
#include "task/temperature.h"
#include "task/pcb_check.h"

TaskManage::TaskManage() : app_state_(kInit), last_app_state_(kInit), task_name_(kNone)
{
}

void TaskManage::Run(_TestSensor *test_data)
{
  switch (app_state_)
  {
  case State::kInit:
  {
    app_state_ = State::kStartup;
    break;
  }
  case State::kStartup:
  {
    app_state_ = State::kWaitUserInput;
    break;
  }
  case State::kWaitUserInput:
  {
    switch (task_name_)
    {
    case kNone:
    {
      break;
    }
    case kHallCalibration:
    {
      task_ = std::make_unique<Calibration>(this);
      app_state_ = State::kRunTask;
      break;
    }
    case kMagnetQc:
    {
      task_ = std::make_unique<MagnetQC>(this);
      app_state_ = State::kRunTask;
      break;
    }
    case kTemperature:
    {
      task_ = std::make_unique<Temperature>(this);
      app_state_ = State::kRunTask;
      break;
    }
    case kPcbCheck:
    {
      task_ = std::make_unique<PcbCheck>(this);
      app_state_ = State::kRunTask;
      break;
    }
    default:
    {
      task_name_ = kNone;
      logger::debug(logger::Level::Info) << "No support this joint task" << std::endl;

      break;
    }
    }
    /* Action for all cases */
    if (task_name_ != kNone)
    {
      message_ = "Ready | 准备中,正在开始";
      app_state_ = State::kEnterTask;
    }
    break;
  }
  case State::kEnterTask:
  {

    TaskBase::RetCode ret_code = task_->Enter();
    if (ret_code == TaskBase::RetCode::kError)
    {
      app_state_ = State::kWaitUserInput;
      logger::debug(logger::Level::Info) << "Task initialization failed, please check the start conditions" << std::endl;
    }
    else if (ret_code == TaskBase::RetCode::kDone)
    {
      message_ = "Test start | 开始测试";
      app_state_ = State::kRunTask;
    }
    break;
  }
  case State::kRunTask:
  {
    TaskBase::RetCode task_ret = task_->Run(test_data);
    if (task_ret == TaskBase::RetCode::kDone || task_ret == TaskBase::RetCode::kError)
    {
      message_ = "Test complete | 测试完成";
      app_state_ = State::kExitTask;
    }
    break;
  }
  case State::kExitTask:
  {
    TaskBase::RetCode ret_code = task_->Exit();
    if (ret_code == TaskBase::RetCode::kDone)
    {
      app_state_ = State::kWaitUserInput;
      /* Reset task variables */
      task_name_ = kNone;
    }
    break;
  }
  default:
    break;
  }

  if (app_state_ != last_app_state_)
  {
    logger::debug(logger::Level::Info) << "App Task State " << kStateName.at(last_app_state_) << "--->>> " << kStateName.at(app_state_);
    last_app_state_ = app_state_;
  }
}

void TaskManage::StartTest(TaskName task_name)
{
  task_name_ = task_name;
}

void TaskManage::StopTest()
{
  app_state_ = State::kExitTask;
  message_ = "Stop test | 测试中断";
}