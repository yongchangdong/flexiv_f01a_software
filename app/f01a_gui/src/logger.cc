/**
 * @file logger.cpp
 * @copyright Flexiv Robotics 2020
 */

#include "logger.h"
#include <spdlog/spdlog.h>

Logger::Logger() = default;

Logger::~Logger() = default;

void Logger::Init(void)
{
    close_file_ = false;
    run_ = true;
    create_file_ = false;
}

void Logger::Run(void)
{
    std::string msg;
    while (run_)
    {
        auto log_data_ = queue_.Front();
        queue_.PopFront();

        if (!log_data_.empty())
        {
            for (auto element : file_header_)
            {
                msg += std::to_string(log_data_[element]) + ",";
            }
            msg.pop_back();

            test_logger_->info(msg);
            msg = "";
        }

        if (close_file_)
        {
            CloseFile();
        }

        if (create_file_)
        {
            CreateFile();
        }
    }

    CloseFile();
    spdlog::info("Background thread stopped");
}

void Logger::CreateFile()
{
    if (test_logger_ != nullptr)
    {
        spdlog::drop("joint_test_logger");
        test_logger_ = nullptr;
        while (queue_.Size() != 0)
        {
            queue_.PopFront();
        }
    }

    test_logger_ = spdlog::basic_logger_mt("joint_test_logger", file_name_, true);
    test_logger_->flush();
    test_logger_->set_pattern("%v");
    std::string header;

    header = "";

    for (auto element : file_header_)
    {
        header += element + ",";
    }
    header.pop_back();

    test_logger_->info(header);

    create_file_ = false;
}

void Logger::CloseFile(void)
{
    if (test_logger_ != nullptr)
    {
        spdlog::drop("joint_test_logger");
        test_logger_ = nullptr;
    }
    close_file_ = false;
}

void Logger::Stop(void)
{
    run_ = false;
    /* these two lines are only to wake up the condition variable in the shared
     * FIFO queue */
    DatabaseData empty;
    queue_.PushBack(empty);
}

void Logger::SetCloseFile(void)
{
    close_file_ = true;
    /* these two lines are only to wake up the condition variable in the shared
     * FIFO queue */
    DatabaseData empty;
    queue_.PushBack(empty);
}

void Logger::SetCreateFile(
    const std::string &file_name, const std::vector<std::string> &file_header)
{
    create_file_ = true;
    file_name_ = file_name;
    file_header_ = file_header;
    /* these two lines are only to wake up the condition variable in the shared
     * FIFO queue */
    DatabaseData empty;
    queue_.PushBack(empty);
}

void Logger::CopyDataToQueue(const DatabaseData &data)
{
    queue_.PushBack(data);
}
