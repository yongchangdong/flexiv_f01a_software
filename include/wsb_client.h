/**
 * @file wsb_client.h
 * @copyright Flexiv Robotics 2021
 * @brief wsb TCP client
 * @author Yongchang Dong
 */
#ifndef WSB_CLIENT_H_
#define WSB_CLIENT_H_

#include "socket.h"
#include "wsb_f.pb-c.h"
#include <thread>
#include <queue>
#include <mutex>
#include <functional>

/** Max protolbuf buff size */
#define MAX_BUFF_SIZE 128
/** Message head flag ( HEAD + Len + Protobuf Data )*/
#define PACK_HEAD 0X55

/** @brief Wsb client,handles application protocols
 */
class WsbClient : public Socket
{
public:
  WsbClient(const Socket &socket);
  ~WsbClient();

  /** @brief Start a background thread to connect a TCP server
   * @return True is start success
   */
  bool Start();

  /** @brief Disconnect and stop background thread
   * @return True is start success
   */
  bool Stop();

  /** @brief Send system comamnd to TCP server
   * @param[in] cmd See System__CmdType
   * @param[in] param_1 First param
   * @param[in] param_2 Second param
   * @param[in] param_3 Third param
   * @param[in] param_4 Fourth param
   * @param[in] param_5 Fifth param
   */
  bool SendSystem(System__CmdType cmd,
                  int32_t param_1 = 0,
                  int32_t param_2 = 0,
                  int32_t param_3 = 0,
                  int32_t param_4 = 0,
                  int32_t param_5 = 0);

  bool SendFtp(uint32_t len, uint8_t *data);

  /** @brief Background thread function definition.
   *  If you don't use threads, call this function directly
   */
  friend int ThreadFun(WsbClient *client);

  /** @brief bind callback function for system data type
   *
   */
  void Register_OnSystem(std::function<void(_System *)>);

  /** @brief bind callback function for data
   *
   */
  void Register_OnData(std::function<void(_Sensor *)>);

  /** @brief bind callback function for test data
   *
   */
  void Register_OnTestData(std::function<void(_TestSensor *)>);

private:
  /** @brief Protobuf package  */
  struct package_t
  {
    struct _WsbMsg wsb_msg;
    struct _System system;
    struct _TestSensor test_sensor;
    struct _Ftp ftp;

    /* Tcp message, Head + Len + Data */
#pragma pack(1)
    struct
    {
      const uint8_t head = PACK_HEAD;
      uint8_t len = 0;
      uint8_t data[MAX_BUFF_SIZE];
    } msg;
#pragma pack()
    /* Recv state machine */
    int unpack_state = 0;
    int data_len;

    /* Protobuf message field */
    uint8_t buff[MAX_BUFF_SIZE]; //
    int buff_len;

    /* */
    bool uesed;
  };

  /** @brief Received TPC data and handle protocol
   * @param[in] package Package instance
   * @param[in] data Tcp data
   */
  bool RecvData(struct package_t *package, uint8_t data);

  /** @brief Pack protobuf msg and save to temporary buff
   * @param[in] package Package instance
   * @param[in] type see WsbMsg__MsgType
   */
  int Pack(struct package_t *package, WsbMsg__MsgType type);

  /** @brief Get protobuf pack type from temporary buff
   * @param[in] package Package instance
   * @return See WsbMsg__MsgType
   */
  WsbMsg__MsgType PackType(struct package_t *package);

  /** @brief UnPack test sensor msg from protobuf data
   * @param[in] package Package instance
   * @return Sensor data
   */
  struct _TestSensor *UnpackTestSensor(struct package_t *package);

  /** @brief UnPack sensor msg from protobuf data
   * @param[in] package Package instance
   * @return Sensor data
   */
  struct _Sensor *UnpackSensor(struct package_t *package);

  /** @brief UnPack system msg from protobuf data
   * @param[in] package Package UnpackSensor
   * @return System data
   */
  struct _System *UnpackSystem(struct package_t *package);

  /** @brief UnPack ftp msg from protobuf data
   * @param[in] package Package instance
   * @return ftp data
   */
  struct _Ftp *UnpackFtp(struct package_t *package);

  /** @brief Received ftp message call back
   * @param[in] ftp ftp data
   */
  void FtpHandle(struct _Ftp *ftp);

  /** @brief Time out call back
   */
  void TimeoutCb();

  struct package_t package_;

  enum class State
  {
    kClose,
    kConnecting,
    kConnected,
  } state_;

  std::thread thread_;
  /* Stop client flag, true if stop is required*/
  bool stop_request_;
  /* callback function */
  std::function<void(_System *)> on_system_;
  std::function<void(_Sensor *)> on_data_;
  std::function<void(_TestSensor *)> on_test_data_;
};

#endif // !CLIENT_H_