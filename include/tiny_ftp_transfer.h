#ifndef TINY_FTP_SERVER_H_
#define TINY_FTP_SERVER_H_

#include <stdint.h>

class WsbClient;

/** @brief Statrt file transer
 * @param[in] client Client instance
 * @param[in] file File name
 */
int TinyFtpTransfer_SendFile(WsbClient *client, const char *file);

/** @brief Process FTP protocol data
 * @param[in] client Client instance
 * @param[in] data Data
 * @param[in] len Data len
 * @return 0 if transfer success
 */
void TinyFtpTransfer_Read(WsbClient *client, uint8_t *data, uint8_t len);

/** @brief Process FTP protocol data
 */
void TinyFtpTransfer_TimePoll();

#endif