#ifndef SOCKET_H_
#define SOCKET_H_

#ifdef WIN32
#include <winsock2.h>
#include <Windows.h>
#else
#include <sys/socket.h>
#endif // WIN32

#include <chrono>
#include <string>
#include <vector>

class Socket
{
public:
  class Address
  {
  public:
    Address();

    Address(const Address &address);

    Address(const ::sockaddr_storage &addr);

    virtual ~Address();

    static Address Ipv4(const ::std::string &string, const unsigned short int &port, const bool &asNumeric = false);

    static Address Ipv4(const ::std::string &string, const ::std::string &port, const bool &asNumeric = false);

    static Address Ipv6(const ::std::string &string, const unsigned short int &port, const bool &asNumeric = false);

    static Address Ipv6(const ::std::string &string, const ::std::string &port, const bool &asNumeric = false);

    const ::sockaddr_storage &get() const;

    ::std::vector<unsigned char> getHexadecimal();

    ::std::string getNameInfo(const bool &asNumeric = false) const;

    bool setInfo(const ::std::string &string, const unsigned short int &port, const bool &asNumeric = false);

    bool setInfo(const ::std::string &string, const ::std::string &port, const bool &asNumeric = false);

  protected:
    Address(const int &family);

  private:
    ::sockaddr_storage addr;
  };

  enum Option
  {
    OPTION_KEEPALIVE,
    OPTION_MULTICAST_LOOP,
    OPTION_MULTICAST_TTL,
#if defined(__APPLE__) || defined(__QNX__) || defined(WIN32)
    OPTION_NODELAY
#else  // __APPLE__ || __QNX__ || WIN32
    OPTION_NODELAY,
    OPTION_QUICKACK
#endif // __APPLE__ || __QNX__ || WIN32
  };

  Socket(const Socket &socket);

  virtual ~Socket();

  static Socket Tcp(const Address &address);

  static Socket Udp(const Address &address);

  Socket accept();

  bool bind();

  bool close();

  bool connect();

  const Address &getAddress() const;

  int getOption(const Option &option) const;

  const int &getProtocol() const;

  const int &getType() const;

  void listen();

  bool listen(const int &backlog);

  bool open();

  ::std::size_t recv(void *buf, const ::std::size_t &count);

  ::std::size_t recvfrom(void *buf, const ::std::size_t &count, Address &address);

  ::std::size_t select(const bool &read, const bool &write, const ::std::chrono::nanoseconds &timeout);

  ::std::size_t send(const void *buf, const ::std::size_t &count);

  ::std::size_t sendto(const void *buf, const ::std::size_t &count, const Address &address);

  void setAddress(const Address &address);

  bool setOption(const Option &option, const int &value);

  bool shutdown(const bool &read = true, const bool &write = true);

  bool isConnected() const;

protected:
  void setConnected(const bool &connected);

protected:
  Socket(const int &type, const int &protocol, const Address &address);

#ifdef WIN32
  Socket(const int &type, const int &protocol, const Address &address, const SOCKET &fd);
#else  // WIN32
  Socket(const int &type, const int &protocol, const Address &address, const int &fd);
#endif // WIN32

#ifdef WIN32
  SOCKET fd;
#else  // WIN32
  int fd;
#endif // WIN32

private:
#ifdef WIN32
  static void cleanup();

  static void startup();
#endif // WIN32

  Address address;

  int protocol;

  int type;

private:
  bool connected;
};

#endif // SOCKET_H_
