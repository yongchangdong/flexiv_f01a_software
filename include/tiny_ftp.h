/**
 * @file tiny_ftp_recv.h
 * @copyright Flexiv Robotics 2022
 * @brief A lightweight file transfer protocol, common definition
 * @author Yongchang Dong
 */
#ifndef TINY_FTP_H_
#define TINY_FTP_H_

#include <stdint.h>

/** Buff max size */
#define kMaxDataSize (128)
/** Time out in 1ms */
#define kTimeOut (2000)

enum FrameType
{
  kFileTransfer = 0,  ///< Request transfer file
  kRequestData,       ///< Request file data
  kData,              ///< Data
  kDone               ///< Done
};

/** @brief Message map for file transfer request data */
struct file_transfer_t
{
  uint32_t file_size; ///< File size
  uint32_t crc;       ///< File crc
  uint8_t name[32];
};

/** @brief Message map for request data */
struct request_data_t
{
  uint32_t index;   ///< Pack index, 0 mean next pack
  uint32_t crc;
};

enum DoneRel
{
  kDoneRel_None = 0,
  kDoneRel_FileToSmall = 1,
  kDoneRel_FileToLarge = 2,
  kDoneRel_FlashFull = 3,
  kDoneRel_FlashWrite = 4,
  kDoneRel_CRC = 5,
  kDoneRel_EEPROM = 6,
};

/** @brief Message map for done respone */
struct done_t
{
  uint32_t result; ///< See DoneRel
  uint32_t crc;
};


/** @brief FTP frame */
struct frame_t
{
  uint8_t frame_type;
  uint8_t data_len;
  uint8_t data[kMaxDataSize];
};

#endif