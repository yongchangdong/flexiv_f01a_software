/**
 * @file coinfig.h.in
 * @copyright Flexiv Robotics 2022
 */
/* clang-format off */

#ifndef FLEXIV_F01A_SOFTWARE_CONFIG_H_
#define FLEXIV_F01A_SOFTWARE_CONFIG_H_

#include <string>

/* clang-format off */

/* CMake configuration of code version */
constexpr unsigned int FLEXIV_F01A_SOFTWARE_VERSION_MAJOR = 1;
constexpr unsigned int FLEXIV_F01A_SOFTWARE_VERSION_MINOR = 0;
constexpr unsigned int FLEXIV_F01A_SOFTWARE_VERSION_PATCH = 1;
const std::string FLEXIV_F01A_SOFTWARE_VERSION =
  "1.0.1";

/* CMake configuration of Git info */
const std::string FLEXIV_F01A_SOFTWARE_GIT_BRANCH = "main";
const std::string FLEXIV_F01A_SOFTWARE_GIT_COMMIT_HASH = "c0f6e636f45b0b225f23c8453dd5f1a2afb6ebb8";

/* clang-format on */

#endif
