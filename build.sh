echo "Cleaning and building ..."

if [ ! -d "./build" ];then
	mkdir ./build
fi

cd build
cmake ../
make install
cd ..
